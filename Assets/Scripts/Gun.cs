﻿using UnityEngine;
using System.Collections;

public class Gun : MonoBehaviour
{
    public GameObject BulletPrefab;
    public float TimeBetweenShots = 0.5f;

    private float _nextFireTime = 0f;

    private void Update()
    {
        if (Input.GetMouseButton(0) && _nextFireTime < Time.time)
        {
            FireBullet();
        }
    }

    private void FireBullet()
    {
        _nextFireTime = Time.time + TimeBetweenShots;

        Vector3 position = transform.position + transform.forward;
        GameObject.Instantiate(BulletPrefab, position, transform.rotation);
    }
}
