﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour
{
    public float Speed = 1f;
    public float TimeToLive = 5f;

    private float _deathTime;

    private void Start()
    {
        _deathTime = Time.time + TimeToLive;
    }

    private void Update()
    {
        if (Time.time > _deathTime)
        {
            GameObject.Destroy(this.gameObject);
            return;
        }

        float distanceToMove = Speed * Time.deltaTime;
        transform.Translate(Vector3.forward * distanceToMove);
    }

    private void OnCollisionEnter(Collision collision)
    {
        var destroyable = collision.gameObject.GetComponent<Destroyable>();
        if (destroyable != null)
        {
            destroyable.Destroy();
        }

        GameObject.Destroy(this.gameObject);
    }
}
